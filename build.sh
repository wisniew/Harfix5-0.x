#!/bin/bash

#    Copyright 2016, 2017 Rafal Wisniewski (wisniew99@gmail.com)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


#############################################
#############################################
##                                         ##
####           Harfix builder            ####
##                                         ##
#############################################
#############################################
##                   1.2                   ##
#                                           #
# Build script for Kernels.                 #
# By wisniew99 / rafciowis1999              #
#                                           #
# Script based on inplementation            #
# in Harfix3 kernel for i9300.              #
#                                           #
# Read and edit all things in tables.       #
#                                           #
##      REMEMBER TO EDIT LOCATIONS!!!      ##
#############################################
#############################################
#                                            #
# If colours looks ugly, use black terminal.  #
#                                              #
#####################################################
###################  MAIN THINGS  ###################
#####################################################
##                                                  ##
PRONAME="Harfix5"         # Project name.             ##
VERSION="0.5"             # Version number or name.     ##
#                                                         ##
#               New name = new main folder                 ##
#############################################################
########################  OPTIONS  ##########################
#############################################################
####              (1-enabled, 0-disabled)                ####
##                                                         ##
CLEAN=0                         # Clean before compile.    ##
##                                                         ##
MAIN=1                          # Compile.                 ##
    MAINZIP=1                   # Zip.                     ##
##                                                         ##
AFTCLEAN=0                      # Clean after compile.     ##
##                                                         ##
####    If compile is disabled, zip is disabled too.     ####
#############################################################
####################  Edit not needed  ######################
#############################################################
##                                                         ##
## Auto detect Your home folder.                           ##
HOME="$(dirname ~)/$(basename ~)"                          ##
##                                                         ##
## Maximum jobs that Your computer can do.                 ##
JOBS="$(grep -c "processor" "/proc/cpuinfo")"              ##
##                                                         ##
## Colors                                                  ##
red=$(tput setaf 1)                 # red     # Error      ##
grn=$(tput setaf 2)                 # green   # Done       ##
ylw=$(tput setaf 11)                # yellow  # Warring    ##
blu=$(tput setaf 4)                 # blue    # path       ##
gren=$(tput setaf 118)              # green   # Name       ##
pur=$(tput setaf 201)               # purple  # Name       ##
txtbld=$(tput bold)                 # Bold    # Info       ##
bldred=${txtbld}$(tput setaf 1)     # Red     # Error desc ##
bldblu=${txtbld}$(tput setaf 4)     # blue    # Info       ##
txtrst=$(tput sgr0)                 # Reset                ##
##                                                         ##
#############################################################
########################  CONFIGS  ##########################
#############################################################
##                                                          ##
##                                                            ##
CONFIG=harfix5_defconfig                 # First config         ##
##                                                                ##
##                                                                 ##
#####################################################################
###########################  EDIT THIS!  ############################
#####################################################################
##                                                                 ##
##                                                                 ##
ARCH=arm64                                       # arch of device  ##
SUBARCH=arm64                                  # subarch of device ##
USER=wisniew99                                   # Name of builder ##
HOST=Harfix-machine                              # name of machine ##
TC_FLAGS_KERNEL="-Wno-maybe-uninitialized"      # Flags for kernel ##
TC_FLAGS_MODULE="-Wno-maybe-uninitialized"     # Flags for modules ##
TCDIR=$HOME/TC                                   # Toolchain dir   ##
TCNAME="google-ndk"                              # Toolchain name  ##
TCEND="bin/aarch64-linux-android-"         # End of toolchain name ##
TCLIB="lib64/"                                  # lib folder in TC ##
##                                                                 ##
##                      ##  TC example:  ##                        ##
##                     $TCDIR/$TCNAME/$TCEND                       ##
##                                                                 ##
##                                                                 ##
#####################################################################
###########################  BUILD PATHS  ###########################
#####################################################################
##                                                                 ##
PATHZIMAGE=""                      # zImage path for first version ##
PATHMODULES="modules"             # modules path for first version ##
##                                                                 ##
##                                                                 ##
##                   ##  Path example:  ##                         ##
##               $PRONAME/ZIP_FILES/PATHZIMAGE                     ##
##               $PRONAME/ZIP_FILES/PATHMODULES                  ##
##                                                             ##
###############################################################
#############################################################

#############
## Welcome ##
#############

echo -e '\0033\0143'
echo ""
echo ""
echo "${gren} Starting Harfix builder...${txtrst}"
echo "${gren} By${txtrst} ${pur}wisniew99 / rafciowis1999 ${txtrst} "
echo "${gren} Program is licenced under GNU GPL v2${txtrst}"
echo ""
echo ""
echo ""

####################
## Create folders ##
####################

echo "${txtbld} Creating folders... ${txtrst}"
echo ""

if [ -e "$PRONAME" ]
then
    echo "${bldblu} $PRONAME${txtrst}${blu} exist. ${txtrst}"
    FIRSTRUN=0
else
    echo "${ylw} $PRONAME not exist. ${txtrst}"
    mkdir $PRONAME
    echo "${grn} Created. ${txtrst}"
    FIRSTRUN=1
fi

if [ -e "$PRONAME/work" ]
then
    echo "${bldblu} $PRONAME/work${txtrst}${blu} exist. ${txtrst}"
else
    echo "${ylw} $PRONAME/work not exist. ${txtrst}"
    mkdir $PRONAME/work
    echo "${grn} Created. ${txtrst}"
fi

if [ -e "$PRONAME/work/boot" ]
then
    echo "${bldblu} $PRONAME/work/boot${txtrst}${blu} exist. ${txtrst}"
else
    echo "${ylw} $PRONAME/work/boot not exist. ${txtrst}"
    mkdir $PRONAME/work/boot
    echo "${grn} Created. ${txtrst}"
fi

if [ -e "$PRONAME/work/modules" ]
then
    echo "${bldblu} $PRONAME/work/modules${txtrst}${blu} exist. ${txtrst}"
else
    echo "${ylw} $PRONAME/work/modules not exist. ${txtrst}"
    mkdir $PRONAME/work/modules
    echo "${grn} Created. ${txtrst}"
fi

if [ -e "$PRONAME/Releases" ]
then
    echo "${bldblu} $PRONAME/Releases${txtrst}${blu} exist. ${txtrst}"
else
    echo "${ylw} $PRONAME/Releases not exist. ${txtrst}"
    mkdir $PRONAME/Releases
    echo "${grn} Created. ${txtrst}"
fi

if [ -e "$PRONAME/ZIP_FILES" ]
then
    echo "${bldblu} $PRONAME/ZIP_FILES${txtrst}${blu} exist. ${txtrst}"
else
    echo "${ylw} $PRONAME/ZIP_FILES not exist. ${txtrst}"
    mkdir $PRONAME/ZIP_FILES
    echo "${grn} Created. ${txtrst}"
fi

if [ -e "$PRONAME/ZIP_FILES/$PATHZIMAGE" ]
then
    echo "${bldblu} $PRONAME/ZIP_FILES/$PATHZIMAGE${txtrst}${blu} exist. ${txtrst}"
else
    echo "${ylw} $PRONAME/ZIP_FILES/$PATHZIMAGE not exist. ${txtrst}"
    mkdir $PRONAME/ZIP_FILES/$PATHZIMAGE
    echo "${grn} Created. ${txtrst}"
fi

if [ -e "$PRONAME/ZIP_FILES/$PATHMODULES" ]
then
    echo "${bldblu} $PRONAME/ZIP_FILES/$PATHMODULES${txtrst}${blu} exist. ${txtrst}"
else
    echo "${ylw} $PRONAME/ZIP_FILES/$PATHMODULES not exist. ${txtrst}"
    mkdir $PRONAME/ZIP_FILES/$PATHMODULES
    echo "${grn} Created. ${txtrst}"
fi

echo ""
echo "${txtbld} Done creating folders. ${txtrst}"
echo ""
echo ""
echo ""

###############
## First run ##
###############

if [ $FIRSTRUN = 1 ]
then
    echo "${txtbld} First run of script detected. ${txtrst}"
    echo ""
    if [ -e "$PRONAME/ZIP_FILES" ]
    then
        echo "${grn} Created folders. ${txtrst}"
    else
        echo "${red} ERROR! ${txtrst}"
        echo "${bldred} Can't create folders. ${txtrst}"
        echo "${bldred} Make them manually ${txtrst}"
        echo "${bldred} or contact with author. ${txtrst}"
        echo ""
        echo ""
        echo ""
        echo "${gren} Harfix builder completed all tasks! ${txtrst}"
        echo ""
        exit 1
    fi
    echo "${txtbld} Put Your installer in ZIP_FILES ${txtrst}"
    echo "${txtbld} and run script again. ${txtrst}"
    echo ""
    echo ""
    echo ""
    echo "${gren} Harfix builder completed all tasks! ${txtrst}"
    echo ""
    exit 0
fi

##################
## Delete files ##
##################

echo "${txtbld} Deleting files... ${txtrst}"
echo ""

if [ -e "$PRONAME/ZIP_FILES/$PRONAME.zip" ]
then
    echo "${ylw} $PRONAME/ZIP_FILES/$PRONAME.zip exist. ${txtrst}"
    rm -rf $PRONAME/ZIP_FILES/$PRONAME.zip
    echo "${grn} Deleted. ${txtrst}"
else
    echo "${bldblu} $PRONAME/ZIP_FILES/$PRONAME.zip${txtrst}${blu} not exist. ${txtrst}"
fi

if [ -e "$PRONAME/work/boot/zImage" ]
then
    echo "${ylw} $PRONAME/work/boot/zImage exist. ${txtrst}"
    rm -rf $PRONAME/work/boot/*
    echo "${grn} Deleted old zImage. ${txtrst}"
else
    echo "${bldblu} $PRONAME/work/boot/*${txtrst}${blu} not exist. ${txtrst}"
fi

if [ -e "$PRONAME/work/modules/dhd.ko" ]
then
    echo "${ylw} $PRONAME/work/modules/dhd.ko exist. ${txtrst}"
    rm -rf $PRONAME/work/modules/*
    echo "${grn} Deleted all modules. ${txtrst}"
else
    echo "${bldblu} $PRONAME/work/modules/*${txtrst}${blu} not exist. ${txtrst}"
fi

if [ -e "$PRONAME/$PRONAME-$VERSION.zip" ]
then
    echo "${ylw} $PRONAME-$VERSION.zip exist. ${txtrst}"
    rm -rf $PRONAME/$PRONAME-$VERSION.zip
    echo "${grn} Deleted. ${txtrst}"
else
    echo "${bldblu} $PRONAME-$VERSION.zip${txtrst}${blu} not exist. ${txtrst}"
fi

if [ -e "arch/$ARCH/boot/zImage" ]
then
    echo "${ylw} arch/$ARCH/boot/zImage exist. ${txtrst}"
    rm -rf arch/$ARCH/boot/zImage
    echo "${grn} Deleted old zImage. ${txtrst}"
else
    echo "${bldblu} arch/$ARCH/boot/zImage${txtrst}${blu} not exist. ${txtrst}"
fi

rm -rf $PRONAME/ZIP_FILES/PATHZIMAGE/*zImage*
rm -rf $PRONAME/ZIP_FILES/PATHZIMAGE/Image
echo "${grn} Deleted old Images. ${txtrst}"

rm -rf $PRONAME/ZIP_FILES/PATHMODULES/*
echo "${grn} Deleted all modules. ${txtrst}"

echo ""
echo "${txtbld} Done deleting files. ${txtrst}"
echo ""
echo ""
echo ""

#############
## Exports ##
#############

export ARCH=$ARCH
export SUBARCH=$SUBARCH
export KBUILD_BUILD_USER=$USER
export KBUILD_BUILD_HOST=$HOST
export CROSS_COMPILE=$TCDIR/$TCNAME/$TCEND
export LD_LIBRARY_PATH=$TCDIR/$TCNAME/$TCLIB
STRIP=$TCDIR/$TCNAME/$TCENDstrip


#######################
## Testing Toolchain ##
#######################

if [ -e $TCDIR/$TCNAME/$TCENDgcc ]
then
    echo "${grn} Toolchain set correctly. ${txtrst}"
    echo ""
    echo ""
    echo ""
else
    echo "${red} ERROR! ${txtrst}"
    echo "${bldred} Toolchain is NOT correct. ${txtrst}"
    echo "${bldred} Change dir or name in script ${txtrst}"
    echo "${bldred} to correctly set up toolchain. ${txtrst}"
    echo ""
    echo ""
    echo ""
    echo "${gren} Harfix builder completed all tasks! ${txtrst}"
    echo ""
    exit 1
fi

###########
## Clean ##
###########

if [ $CLEAN = 1 ]
then
    echo "${bldblu} Cleaning... ${txtrst}"
    make -j "$JOBS" clean
    make -j "$JOBS" mrproper
    echo "${grn} Cleaned. ${txtrst}"
    echo ""
    echo ""
    echo ""
fi


###############
## Compiling ##
###############

echo "${txtbld} Starting compile... ${txtrst}"
echo ""
echo ""
echo ""

echo "${bldblu} Loading config... ${txtrst}"
make $CONFIG
echo "${grn} Done. ${txtrst}"
echo ""

echo "${bldblu} Compiling... ${txtrst}"
make -j "$JOBS" CFLAGS_KERNEL="$TC_FLAGS_KERNEL" CFLAGS_MODULE="$TC_FLAGS_MODULE"
echo "${grn} Done. ${txtrst}"
echo ""

if [ $ARCH = "arm64" ]
then
    if [ -e "arch/$ARCH/boot/Image.gz-dtb" ]
    then
        mv arch/$ARCH/boot/Image.gz-dtb arch/$ARCH/boot/zImage
    else
        mv arch/$ARCH/boot/Image.gz arch/$ARCH/boot/zImage
    fi
fi

if [ -e "arch/$ARCH/boot/zImage" ]
then
    echo "${bldblu} Coping modules... ${txtrst}"
    find -name '*.ko' -exec cp -av {} $PRONAME/work/modules/ \;
    echo "${grn} Done. ${txtrst}"
    echo ""

    echo "${bldblu} Coping zImage... ${txtrst}"
    cp arch/$ARCH/boot/zImage $PRONAME/work/boot/
    echo "${grn} Done. ${txtrst}"
    echo ""
    echo ""
    echo ""

    echo "${grn} zImage detected. ${txtrst}"
    echo ""
    echo ""
    echo ""

    if [ $MAINZIP = 1 ]
    then

        echo "${txtbld} Starting compress... ${txtrst}"
        echo ""
        echo ""
        echo ""

        echo "${bldblu} Coping files for zip... ${txtrst}"
        cp $PRONAME/work/modules/* $PRONAME/ZIP_FILES/$PATHMODULES/
        cp $PRONAME/work/boot/zImage $PRONAME/ZIP_FILES/$PATHZIMAGE/
        echo "${grn} Done. ${txtrst}"
        echo ""

        echo "${bldblu} Zipping... ${txtrst}"
        cd $PRONAME/ZIP_FILES
        zip -r $PRONAME.zip *
        cd -
        echo "${grn} Done. ${txtrst}"
        echo ""

        echo "${bldblu} Moving... ${txtrst}"
        mv $PRONAME/ZIP_FILES/$PRONAME.zip $PRONAME/Releases/
        echo "${grn} Done. ${txtrst}"
        echo ""

        echo "${bldblu} Renaming... ${txtrst}"
        mv $PRONAME/Releases/$PRONAME.zip $PRONAME/Releases/$PRONAME-$VERSION.zip
        echo "${grn} Done. ${txtrst}"
        echo ""
        echo ""
        echo ""
        echo "${txtbld} Done build compile with compress. ${txtrst}"
    else
        echo "${txtbld} Done build compile. ${txtrst}"
    fi
else
    echo "${red} ERROR! ${txtrst}"
    echo "${bldred} zImage NOT detected. ${txtrst}"
    echo ""
    echo ""
    echo ""
    echo "${gren} Harfix builder completed all tasks! ${txtrst}"
    echo ""
    exit 1
fi
echo ""
echo ""
echo ""

rm -rf arch/$ARCH/boot/zImage
rm -rf $PRONAME/$ZIP_FILES/$PATHZIMAGE/*zImage*
rm -rf $PRONAME/$ZIP_FILES/$PATHZIMAGE/Image
rm -rf $PRONAME/$ZIP_FILES/$PATHMODULES/*
rm -rf $PRONAME/work/boot/*
rm -rf $PRONAME/work/modules/*

################
# Latest clean #
################

if [ $AFTCLEAN = 1 ]
then
    echo "${bldblu} Cleaning... ${txtrst}"
    make -j "$JOBS" clean
    make -j "$JOBS" mrproper
    echo "${grn} Cleaned. ${txtrst}"
    echo ""
    echo ""
    echo ""
fi

rm -rf $PRONAME/work


echo "${gren} Harfix builder completed all tasks! ${txtrst}"
echo ""
exit 0

